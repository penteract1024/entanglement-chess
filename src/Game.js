import { INVALID_MOVE } from 'boardgame.io/core';

const deepcopy = require('deepcopy');

const movePos = {
  p: [],
  b: [],
  n: [
    [1,2],
    [2,1],
    [-1,2],
    [-2,1],
    [1,-2],
    [2,-1],
    [-1,-2],
    [-2,-1],
  ],
  r: [],
  q: [],
  k: [
    [0,1],
    [1,1],
    [1,0],
    [1,-1],
    [0,-1],
    [-1,-1],
    [-1,0],
    [-1,1],
  ],
};

const moveVec = {
  p: [],
  b: [
    [1,1],
    [1,-1],
    [-1,-1],
    [-1,1],
  ],
  n: [],
  r: [
    [0,1],
    [1,0],
    [0,-1],
    [-1,0],
  ],
  q: [
    [0,1],
    [1,1],
    [1,0],
    [1,-1],
    [0,-1],
    [-1,-1],
    [-1,0],
    [-1,1],
  ],
  k: [],
};
const nonKings="bnrqp"
const typeIndices=[...Array(8).keys()]
const pawnTypeIndices=[4,5,6,7]
// bnrq(pb)(pn)(pr)(pq)
// last 4 are Pawn promoting to each of bnrq
const promoteOptions = "bnrq"

const counts = {
  p: 8,
  b: 2,
  n: 2,
  r: 2,
  q: 1
}

const initialSubsetsFixed = Array(256).fill([0x80000000,0x80000000])

const moveIdGen = (src, dest) => {
  let res = '';
  res += String.fromCharCode(97 + src[1]);
  res += (src[0] + 1);
  res += String.fromCharCode(97 + dest[1]);
  res += (dest[0] + 1);
  return res;
}

function* getReverseHits(G, piece, type){
  let brd = G.brd
  let col=brd[piece[0]][piece[1]]>=16;
  function isCapture(dest){
    return (dest[0]<8 && dest[0]>=0 && dest[1]>=0 && dest[1]<8)
            && (brd[dest[0]][dest[1]]!=-1)
            && ((brd[dest[0]][dest[1]]>=16) != col)

  }
  function isSpace(dest){
    return (dest[0]<8 && dest[0]>=0 && dest[1]>=0 && dest[1]<8 && brd[dest[0]][dest[1]]==-1 )
  }
  for(const pos of movePos[type]) {
    let dest = [
      piece[0] + pos[0],
      piece[1] + pos[1]
    ];
    if(isCapture(dest)){
      yield brd[dest[0]][dest[1]];
    }
  }
  for(const vec of moveVec[type]) {
    let dest = piece.slice();
    while(true) {
      dest[0] = dest[0] + vec[0];
      dest[1] = dest[1] + vec[1];
      if(isCapture(dest)){
        yield brd[dest[0]][dest[1]];
      }
      if (!isSpace(dest)) break;
    }
  }
  if(type === 'p') {
    //Single square forward movement
    let dest = [
      piece[0] + (col ? -1 : 1),
      piece[1] + 1
    ];
    //Captures
    if(isCapture(dest)){
      let pieceId = brd[dest[0]][dest[1]];
      if(!G.promoted[pieceId]) yield pieceId;
    }
    dest[1] = dest[1] - 2;
    if(isCapture(dest)){
      let pieceId = brd[dest[0]][dest[1]];
      if(!G.promoted[pieceId]) yield pieceId;
    }
  }
}

const pieceMoveGen = (G, piece) => {
  let moves = {};
  let piecePossibilities = G.board[piece[0]][piece[1]];
  let col = (G.brd[piece[0]][piece[1]]>=16);
  function inBounds(dest){
    return (dest[0]<8 && dest[0]>=0 && dest[1]>=0 && dest[1]<8)
  }
  function isSpace(dest){
    return (dest[0]<8 && dest[0]>=0 && dest[1]>=0 && dest[1]<8 && G.board[dest[0]][dest[1]]===null )
  }
  const destProcess = (dest, piecePossibility) => {
    if(!inBounds(dest)) return;
    let destId = G.brd[dest[0]][dest[1]];
    if((destId!=-1) && ((destId>=16)==col)) return; // Can't move onto own piece
    let moveId = moveIdGen(piece, dest);
    if(moves[moveId]===undefined){
      moves[moveId]={
        id: moveId,
        from: piece.slice(),
        to: dest.slice(),
        possiblePiece: [piecePossibility]
      }
    }
    else {
      moves[moveId].possiblePiece.push(piecePossibility);
    }
  };
  for(const piecePossibility of piecePossibilities.flat()) {
    for(const pos of movePos[piecePossibility.toLowerCase()]) {
      let dest = [
        piece[0] + pos[0],
        piece[1] + pos[1]
      ];
      destProcess(dest, piecePossibility)
    }
    for(const vec of moveVec[piecePossibility.toLowerCase()]) {
      let dest = piece.slice();
      while(true) {
        dest[0] = dest[0] + vec[0];
        dest[1] = dest[1] + vec[1];
        destProcess(dest, piecePossibility)
        if (!isSpace(dest)) break;
      }
    }
    if(piecePossibility.toLowerCase() === 'p') {
      //Single square forward movement
      let dest = [
        piece[0] + (piecePossibility === 'p' ? -1 : 1),
        piece[1] + 1
      ];
      //Captures
      if(!isSpace(dest)) destProcess(dest, piecePossibility)
      dest[1] = dest[1] - 2;
      if(!isSpace(dest)) destProcess(dest, piecePossibility)
      dest[1] = dest[1] + 1;
      if (isSpace(dest)){
        destProcess(dest, piecePossibility)
        if (G.unmoved[piece[0]][piece[1]] === 1) {
          dest[0] = dest[0] + (piecePossibility === 'p' ? -1 : 1);
          if(isSpace(dest)) destProcess(dest, piecePossibility)
        }
      }
    }
  }
  let ret=[]
  for(let k in moves) ret.push(moves[k])
  return ret;
}

const moveGen = (G, currentPlayer) => {
  let moves = [];
  for(let r = 0;r < 8;r++) {
    for(let f = 0;f < 8;f++) {
      let piecePossibilities = G.board[r][f];
      if(piecePossibilities !== null && piecePossibilities.length > 0){
        if(
          (currentPlayer === '0' && piecePossibilities.flat()[0] === piecePossibilities.flat()[0].toUpperCase()) ||
          (currentPlayer === '1' && piecePossibilities.flat()[0] === piecePossibilities.flat()[0].toLowerCase())
        ) {
          let pieceMoves = pieceMoveGen(G, [r,f]);
          for(let pieceMove of pieceMoves) {
            moves.push(pieceMove);
          }
        }
      }
    }
  }
  return moves;
}

const moveExec = (G, move, log) => {
  let movingPieceId = G.brd[move.from[0]][move.from[1]];
  let bit = 1<<movingPieceId;
  let promoted = G.promoted[movingPieceId];
  G.board[move.from[0]][move.from[1]] = null;
  G.unmoved[move.to[0]][move.to[1]] = 0;


  //Update allowed piece types for moving piece:
  let unset=[]; // the set of pieces that the moving piece cannot be
  let possibs = move.possiblePiece.map(x=>x.toLowerCase())
  for(let pieceLetter of nonKings){
    if (!possibs.includes(pieceLetter)) unset.push(pieceLetter)
  }
  let includesK = possibs.includes("k")
  for (let idx of G.allowedKs){
    let i = idx>>4;
    let j = idx&0xF;

    //let idx=i*16+j;
    // If the moving piece cannot be a king, but would be in this universe,
    // then rule out this universe
    if((i==movingPieceId || j+16==movingPieceId) && !includesK){
      for(let pieceType of typeIndices){
        G.allowed[idx][pieceType]=0;
      }
    }
    else{
      for(let pieceLetter of unset){
        if(pieceLetter=="p" && !promoted){
          for (let i of pawnTypeIndices){
            G.allowed[idx][i]&=~bit
          }
        }
        else if (pieceLetter!="p"){
          let pieceType=promoteOptions.search(pieceLetter)
          G.allowed[idx][pieceType]&=~bit
          if(promoted) G.allowed[idx][4+pieceType]&=~bit
        }
      }
    }
  }
  if(G.brd[move.to[0]][move.to[1]]!=-1){
    G.cptrd.push(G.brd[move.to[0]][move.to[1]])
  }
  G.brd[move.to[0]][move.to[1]]=G.brd[move.from[0]][move.from[1]];
  G.brd[move.from[0]][move.from[1]]=-1;

  let col = movingPieceId>=16;//black=true
  if(move.to[0]==7-7*col) {
    G.promoted[movingPieceId]=true;
  }
  //update allowed piece types for enemy pieces
  noChecks(G,col)
}

// Add constraints to G asserting that player col is not in check
function noChecks(G,col){
  for(let a=0;a<16;a++){
    let kingId = a+col*16 // this is the king belonging to the player who must not be in check
    let kingPos=null
    for (let x=0; kingPos===null && x<8; x++){
      for (let y=0; kingPos===null && y<8; y++){
        if (G.brd[x][y]==kingId){
          kingPos=[x,y];
        }
      }
    }
    if(kingPos===null){
      continue;
    }
    //with king a fixed, which pieces are allowed to be what based on checks
    // (allowed[i]&(1<<pieceId)!=0) means pieceId is allowed to be "bnrqppppk"[i]
    let allowed = []
    let notcheckQ=0xFFFFFFFF
    for (let pieceLetter of (nonKings+"k")){
      let notcheck=0xFFFFFFFF
      if(pieceLetter=="q") notcheck=notcheckQ;
      else{
        for(let id of getReverseHits(G,kingPos,pieceLetter)){
          notcheck&=~(1<<id)
        }
        if(pieceLetter=="b" || pieceLetter=="r"){
          notcheckQ&=notcheck
        }
      }
      allowed.push(notcheck)
      if(pieceLetter=="p"){
        allowed.push(notcheck)
        allowed.push(notcheck)
        allowed.push(notcheck)
      }
    }
    for(let b=0;b<16;b++){
      let idx = a*(16-col*15)+b*(1+col*15)//a*(1+col*15)+b*(16-col*15)
      let otherKingId = b+(1-col)*16
      if(allowed[8]&(1<<otherKingId)){//king is allowed here
        for (let pieceType of typeIndices){
          G.allowed[idx][pieceType]&=allowed[pieceType]
        }
      }
      else{
        for(let pieceType of typeIndices){
          G.allowed[idx][pieceType]=0;
        }
      }
    }
  }
}

function bitCount (n) {
  n = n - ((n >> 1) & 0x55555555)
  n = (n & 0x33333333) + ((n >> 2) & 0x33333333)
  return ((n + (n >> 4) & 0xF0F0F0F) * 0x1010101) >> 24
}

// Uses Hall's marriage theorem to tell if there is an assignment of piece types to
// pieces satisfying the constraints
function isLegal(allowed,allowedKs,log){
  for (let idx of allowedKs){
    let flag = true
    for(let subset=32;subset>0;subset--){//32 == (1<<nonKings.length)
      //Deliberately skipping 0; counting down for efficiency
      let included = 0;//bitmask
      let required = 0;//at least this many pieces are needed
      for(let i=0; i<nonKings.length; i++) if (subset&(1<<i)){
        included|=allowed[idx][i];
        if(nonKings[i]=="p"){
          included|=allowed[idx][i+1];
          included|=allowed[idx][i+2];
          included|=allowed[idx][i+3];
        }
        required+=counts[nonKings[i]];
      }
      let cw = bitCount(included&0xFFFF)
      let cb = bitCount((included>>16)&0xFFFF)
      if((cw<required) || (cb<required)) {
        flag = false;
        break;
      }
    }
    if(flag)return true;
  }
  return false;
}

// Update G.board based on G.brd and G.allowed
// Also make some changes which make future computations easier
function drawPieces(G){
  let subsetsFixed=[]
  for (let s1 of initialSubsetsFixed){
    let r1=[]
    for(let s2 of s1){
      r1.push(s2)
    }
    subsetsFixed.push(r1)
  }
  // Update the set of king configurations that are still possible
  let nextAllowedKs=[]
  for (let idx of G.allowedKs){
    let flag=true;
    for(let subset=1;subset<32;subset++){//32 == (1<<nonKings.length)
      let included = 0;//bitmask
      let required = 0;//at least this many pieces are needed
      for(let i=0; i<nonKings.length; i++) if (subset&(1<<i)){
        included|=G.allowed[idx][i];
        if(nonKings[i]=="p"){
          included|=G.allowed[idx][i+1];
          included|=G.allowed[idx][i+2];
          included|=G.allowed[idx][i+3];
        }
        required+=counts[nonKings[i]];
      }
      let cw = bitCount(included&0xFFFF)
      let cb = bitCount((included>>16)&0xFFFF)
      if((cw<required) || (cb<required)) {
        flag=false;
        break;
      }
    }
    if(flag)nextAllowedKs.push(idx);
  }
  G.allowedKs=nextAllowedKs

  // Eliminate things we know can't be have some type because the piece must have some other type
  // (e.g. 3 pieces have made 2-space moves diagonally, there are only 12 non-king pieces
  //    that can be pawns, knights or rooks, so those 12 cannot be bishops or queens)
  // By a corollary of Hall's marriage theorem,
  //   if this process does not rule a piece out of having a particular type,
  //     then there is some configuration where it has that type.
  for (let idx of G.allowedKs){
    for(let col of [0,1]){
      let changed=true
      while(changed){
        changed=false
        for(let subset=1;subset<32;subset++)if(!(subsetsFixed[idx][col]&(1<<subset))){//32 == (1<<nonKings.length)
          let included = 0;//bitmask
          let required = 0;//at least this many pieces are needed
          for(let i=0; i<nonKings.length; i++) if (subset&(1<<i)){
            included|=G.allowed[idx][i];
            if(nonKings[i]=="p"){
              included|=G.allowed[idx][i+1];
              included|=G.allowed[idx][i+2];
              included|=G.allowed[idx][i+3];
            }
            required+=counts[nonKings[i]];
          }
          let includedCol = included&(col?0xFFFF0000:0xFFFF)
          let cw = bitCount(includedCol)
          if(cw==required){
            for(let i=0; i<nonKings.length; i++) if (!(subset&(1<<i))){
              G.allowed[idx][i] &= ~includedCol
              if(nonKings[i]=="p"){
                G.allowed[idx][i+1] &= ~includedCol
                G.allowed[idx][i+2] &= ~includedCol
                G.allowed[idx][i+3] &= ~includedCol
              }
            }
            subsetsFixed[idx][col]|=(1<<subset)
            changed=true
          }
        }
      }
    }
  }
  //Finally do the work we care about:
  let possibs = Array(32).fill(0) //options for each of the 32 pieces
  for (let idx of G.allowedKs){
    let bk = (idx&0xF) + 16
    let wk = idx>>4
    possibs[bk]|=1<<typeIndices.length
    possibs[wk]|=1<<typeIndices.length
    for(let typeIndex of typeIndices){
      let x = G.allowed[idx][typeIndex]
      for(let pieceId=0;pieceId<32;pieceId++){
        if(x&(1<<pieceId))possibs[pieceId]|=1<<typeIndex;
      }
    }
  }
  let sb = (nonKings+"pppk")
  let sw = sb.toUpperCase()
  for(let x=0;x<8;x++)for(let y=0;y<8;y++){
    let pieceId = G.brd[x][y];
    let pieceLetters=(pieceId>=16?sb:sw)
    if(pieceId!==-1){
      let possibTypes = possibs[pieceId]
      let possibNames = []
      for(let i of [0,1,2,3,8]){
        if(possibTypes&(1<<i))possibNames.push(pieceLetters[i])
      }
      if(G.promoted[pieceId] && (possibTypes&0xF0)){
        let possibPromotedNames = []
        for(let i of pawnTypeIndices){
          if(possibTypes&(1<<i)) possibPromotedNames.push(pieceLetters[i-4])
        }
        possibNames.push(possibPromotedNames)
      }
      else{
        if(possibTypes&(1<<4)){
          possibNames.push(pieceLetters[4])
        }
      }
      G.board[x][y] = possibNames
    }
  }
  G.captured=[]
  for(let p of G.cptrd){
    let possibNames = []
    for(let i=0; i<9; i++){
      if(possibs[p]&(1<<i)) possibNames.push((p>=16?sb:sw)[i])
    }
    G.captured.push(possibNames)
  }
  return false;
}

const moveIsValid = (G, move) => {
  let newG = copyG(G);
  moveExec(newG, move);
  return isLegal(newG.allowed,G.allowedKs);
}

const checkingNextPlayer = (G, nextPlayer) => {
  //Is the situation check - i.e. is it check for all possible permutations
  let newG = copyG(G)
  noChecks(newG, nextPlayer=="0")
  return !isLegal(newG.allowed,newG.allowedKs)
}


const copyPPs = (PPs) => {
  return PPs.slice()
}

const copyG = (G) => {
  let newG = {};
  //board, moves, validMoves and captured will be filled in by other functions (such as drawPieces) if needed
  newG.board = [];
  for(let r = 0;r < 8;r++) {
    newG.board[r] = [];
    for(let f = 0;f < 8;f++) {
      newG.board[r][f] = null;
    }
  }
  newG.moves = [];
  newG.captured = [];

  newG.unmoved = G.unmoved.map(x=>x.slice());
  //moveHistory and moveHistoryWasCapture will be added by EntanglementChess.moves.movePiece.move if needed

  newG.inCheck = G.inCheck;
  newG.brd = G.brd.map(x=>x.slice())
  newG.allowed = G.allowed.map(x=>x.slice())

  newG.cptrd = G.cptrd.slice();
  newG.promoted=G.promoted.slice();
  newG.allowedKs = G.allowedKs.slice();
  return newG;
}

export const EntanglementChess = {
  name: 'entanglement-chess',
  setup: (ctx) => {
    let wP = ['P','B','N','R','Q','K'];
    let bP = ['p','b','n','r','q','k'];
    let eight = [...Array(8).keys()];
    let G = {
      board: [
        [wP.slice(),wP.slice(),wP.slice(),wP.slice(),wP.slice(),wP.slice(),wP.slice(),wP.slice()],
        [wP.slice(),wP.slice(),wP.slice(),wP.slice(),wP.slice(),wP.slice(),wP.slice(),wP.slice()],
        [null,null,null,null,null,null,null,null],
        [null,null,null,null,null,null,null,null],
        [null,null,null,null,null,null,null,null],
        [null,null,null,null,null,null,null,null],
        [bP.slice(),bP.slice(),bP.slice(),bP.slice(),bP.slice(),bP.slice(),bP.slice(),bP.slice()],
        [bP.slice(),bP.slice(),bP.slice(),bP.slice(),bP.slice(),bP.slice(),bP.slice(),bP.slice()],
      ],
      brd: [
        eight,
        eight.map(x=>x+8),
        Array(8).fill(-1),
        Array(8).fill(-1),
        Array(8).fill(-1),
        Array(8).fill(-1),
        eight.map(x=>x+16),
        eight.map(x=>x+24),
      ],
      captured: [],
      cptrd:[],
      unmoved: [
        [1,1,1,1,1,1,1,1],
        [1,1,1,1,1,1,1,1],
        [0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0],
        [1,1,1,1,1,1,1,1],
        [1,1,1,1,1,1,1,1],
      ],
      allowed: [], // 256 * 5 32-bit vectors representing the set of pieces allowed to be a particular type
      allowedKs: [...Array(256).keys()],// a list of indices indicating which king positions are left
      promoted: Array(32).fill(false),
      moves: [],
      moveHistory: [],
      moveHistoryWasCapture: [],
      inCheck: false, //In check on all boards
      noLegalMoves: false,
    };
    for (let i=0;i<16;i++){// white king pos
      let wk = 0xFFFF & (~(1<<i));
      for (let j=0;j<16;j++){
        let bk = 0xFFFF & ~(1<<j);
        let notKings = wk | (bk<<16);
        G.allowed.push(Array(8).fill(notKings));
      }
    }
    noChecks(G,true); // assert that black is not in check in the starting position
    drawPieces(G);
    let moves = moveGen(G, ctx.currentPlayer);
    G.moves = moves;
    return G;
  },
  moves: {
    movePiece: {
      move: (G, ctx, moveId) => {
        let selectedMove = null;
        for(const move of G.moves) {
          if(move.id === moveId) {
            selectedMove = deepcopy(move);
          }
        }
        if(selectedMove === null) {
          return INVALID_MOVE;
        }
        let newG = copyG(G);
        if(!moveIsValid(newG,selectedMove)){
          return INVALID_MOVE;
        }
        newG.moveHistory = G.moveHistory.slice()
        newG.moveHistory.push(selectedMove.id);
        moveExec(newG, selectedMove, true);
        drawPieces(newG);
        newG.moveHistoryWasCapture = G.moveHistoryWasCapture.slice()
        newG.moveHistoryWasCapture.push(newG.captured.length > G.captured.length);
        newG.inCheck = checkingNextPlayer(newG, ctx.currentPlayer);
        let nextPlayer = ctx.currentPlayer === '0' ? '1' : '0';
        let newMoves = moveGen(newG, nextPlayer);
        newG.noLegalMoves=true
        for(const newMove of newMoves) {
          if(moveIsValid(newG, newMove)) {
            newG.noLegalMoves=false
            break;
          }
        }
        newG.moves = newMoves;
        return newG;
      },
      undoable: false,
      redact: false,
      client: true,
      noLimit: false,
      ignoreStaleStateID: false,
    }
  },
  turn: { moveLimit: 1 },
  minPlayers: 2,
  maxPlayers: 2,
  endIf: (G, ctx) => {
    if(G.noLegalMoves) {
      if(G.inCheck) {
        return { winner: ctx.currentPlayer };
      }
      return { draw: true };
    }
  },
  ai: {
    enumerate: (G, ctx) => {
      let newG = copyG(G);
      let validMoves = [];
      for(const newMove of G.moves) {
        if(moveIsValid(newG, newMove)) {
          validMoves.push(newMove);
        }
      }
      return validMoves.map(move => ({ move: 'movePiece', args: [move.id] }));
    }
  }
}
