import * as PIXI from 'pixi.js';

/*
{
  clickable: boolean
}
*/
export default class Highlight {
  constructor(emitter, x, y, parentContainer, id = null, highlight = null) {
    this.emitter = emitter;
    this.position = new PIXI.Point();
    this.position.set(x, y);
    this.root = new PIXI.Container();
    this.root.width = 100;
    this.root.height = 100;
    this.root.alpha = 0.3;
    this.root.position = this.position;
    parentContainer.addChild(this.root);
    this.graphics = new PIXI.Graphics();
    this.graphics.interactive = true;
    this.root.addChild(this.graphics);
    this.id = id;
    this.listen();
    this.update(highlight);
  }
  update(highlight) {
    if(highlight === null) {
      this.root.visible = false;
    }
    else {
      //Start drawing
      this.graphics.clear();
      this.graphics.beginFill(highlight.clickable ? 0x009900 : 0x00cccc);
      this.graphics.drawRect(0, 0, 100, 100);
      this.graphics.endFill();
      this.root.visible = true;
    }
  }
  listen() {
    this.graphics.on('pointertap', () => {
      this.emitter.emit('highlightSelect', this.id);
    });
  }
}