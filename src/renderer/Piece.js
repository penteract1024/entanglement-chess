import * as PIXI from 'pixi.js';
import '@pixi/graphics-extras';
//import { RGBSplitFilter } from 'pixi-filters';

//Taken from https://github.com/pixijs/pixijs/issues/1333#issuecomment-379000150
PIXI.Graphics.prototype.drawDashedPolygon = function(polygons, x, y, rotation, dash, gap, offsetPercentage){
  var i;
  var p1;
  var p2;
  var dashLeft = 0;
  var gapLeft = 0;
  if(offsetPercentage>0){
    var progressOffset = (dash+gap)*offsetPercentage;
    if(progressOffset < dash) dashLeft = dash-progressOffset;
    else gapLeft = gap-(progressOffset-dash);
  }
  var rotatedPolygons = [];
  for(i = 0; i<polygons.length; i++){
    var p = {x:polygons[i].x, y:polygons[i].y};
    var cosAngle = Math.cos(rotation);
    var sinAngle = Math.sin(rotation);
    var dx = p.x;
    var dy = p.y;
    p.x = (dx*cosAngle-dy*sinAngle);
    p.y = (dx*sinAngle+dy*cosAngle);
    rotatedPolygons.push(p);
  }
  for(i = 0; i<rotatedPolygons.length; i++){
    p1 = rotatedPolygons[i];
    if(i == rotatedPolygons.length-1) p2 = rotatedPolygons[0];
    else p2 = rotatedPolygons[i+1];
    var dx = p2.x-p1.x;
    var dy = p2.y-p1.y;
    var len = Math.sqrt(dx*dx+dy*dy);
    var normal = {x:dx/len, y:dy/len};
    var progressOnLine = 0;
    this.moveTo(x+p1.x+gapLeft*normal.x, y+p1.y+gapLeft*normal.y);
    while(progressOnLine<=len){
      progressOnLine+=gapLeft;
      if(dashLeft > 0) progressOnLine += dashLeft;
      else progressOnLine+= dash;
      if(progressOnLine>len){
        dashLeft = progressOnLine-len;
        progressOnLine = len;
      }else{
        dashLeft = 0;
      }
      this.lineTo(x+p1.x+progressOnLine*normal.x, y+p1.y+progressOnLine*normal.y);
      progressOnLine+= gap;
      if(progressOnLine>len && dashLeft == 0){
        gapLeft = progressOnLine-len;
      }else{
        gapLeft = 0;
        this.moveTo(x+p1.x+progressOnLine*normal.x, y+p1.y+progressOnLine*normal.y);
      }
    }
  }
}

export default class Piece {
  constructor(emitter, x, y, parentContainer, id = null, piecePossibilities = null) {
    this.emitter = emitter;
    this.position = new PIXI.Point();
    this.position.set(x, y);
    this.root = new PIXI.Container();
    this.root.width = 100;
    this.root.height = 100;
    this.root.position = this.position;
    //this.root.filters = [new RGBSplitFilter([0,0],[0,0],[0,0])];
    parentContainer.addChild(this.root);
    this.graphics = new PIXI.Graphics();
    this.graphics.interactive = true;
    this.root.addChild(this.graphics);
    this.promotedGraphics = new PIXI.Graphics();
    this.promotedGraphics.filters = [new PIXI.filters.AlphaFilter(1)];
    this.root.addChild(this.promotedGraphics);
    this.id = id;
    this.possibilityLength = 0;
    this.hasNonPromoted = true;
    this.hasPromoted = false;
    this.listen();
    this.update(piecePossibilities);
  }
  update(piecePossibilities) {
    if(Array.isArray(piecePossibilities) && piecePossibilities.length > 0) {
      //Extract info
      let isWhite = piecePossibilities.flat()[0] === piecePossibilities.flat()[0].toUpperCase();
      let possiblePieces = [];
      for(let piecePossibility of piecePossibilities) {
        if(!Array.isArray(piecePossibility)) {
          possiblePieces.push(piecePossibility.toLowerCase());
        }
      }
      this.hasPromoted = false;
      let possiblePromotedPieces = [];
      for(let piecePossibility of piecePossibilities) {
        if(Array.isArray(piecePossibility)) {
          this.hasPromoted = true;
          for(let promotedPiecePossibility of piecePossibility) {
            possiblePromotedPieces.push(promotedPiecePossibility.toLowerCase());
          }
        }
      }
      this.possibilityLength = possiblePieces.length + possiblePromotedPieces.length;
      this.hasNonPromoted = possiblePieces.length > 0;
      //Start drawing
      this.graphics.clear();
      this.graphics.lineStyle({
        width: 3.8,
        color: isWhite ? 0x000000 : 0xffffff,
        cap: PIXI.LINE_CAP.ROUND,
        join: PIXI.LINE_JOIN.ROUND
      });
      this.graphics.beginFill(isWhite ? 0xffffff : 0x000000);
      this.graphics.drawCircle(50, 50, 45);
      this.graphics.endFill();
      if(possiblePieces.includes('p')) {
        this.graphics.drawCircle(50, 50, 34);
      }
      if(possiblePieces.includes('n')) {
        this.graphics.moveTo(
          50 + Math.cos(Math.atan2(2,3)) * 24,
          50 + Math.sin(Math.atan2(2,3)) * 24
        );
        this.graphics.lineTo(
          50 + Math.cos(Math.atan2(2,3)) * 34,
          50 + Math.sin(Math.atan2(2,3)) * 34
        );
        this.graphics.moveTo(
          50 - Math.cos(Math.atan2(2,3)) * 24,
          50 + Math.sin(Math.atan2(2,3)) * 24
        );
        this.graphics.lineTo(
          50 - Math.cos(Math.atan2(2,3)) * 34,
          50 + Math.sin(Math.atan2(2,3)) * 34
        );
        this.graphics.moveTo(
          50 + Math.cos(Math.atan2(2,3)) * 24,
          50 - Math.sin(Math.atan2(2,3)) * 24
        );
        this.graphics.lineTo(
          50 + Math.cos(Math.atan2(2,3)) * 34,
          50 - Math.sin(Math.atan2(2,3)) * 34
        );
        this.graphics.moveTo(
          50 - Math.cos(Math.atan2(2,3)) * 24,
          50 - Math.sin(Math.atan2(2,3)) * 24
        );
        this.graphics.lineTo(
          50 - Math.cos(Math.atan2(2,3)) * 34,
          50 - Math.sin(Math.atan2(2,3)) * 34
        );
        this.graphics.moveTo(
          50 + Math.cos(Math.atan2(3,2)) * 24,
          50 + Math.sin(Math.atan2(3,2)) * 24
        );
        this.graphics.lineTo(
          50 + Math.cos(Math.atan2(3,2)) * 34,
          50 + Math.sin(Math.atan2(3,2)) * 34
        );
        this.graphics.moveTo(
          50 - Math.cos(Math.atan2(3,2)) * 24,
          50 + Math.sin(Math.atan2(3,2)) * 24
        );
        this.graphics.lineTo(
          50 - Math.cos(Math.atan2(3,2)) * 34,
          50 + Math.sin(Math.atan2(3,2)) * 34
        );
        this.graphics.moveTo(
          50 + Math.cos(Math.atan2(3,2)) * 24,
          50 - Math.sin(Math.atan2(3,2)) * 24
        );
        this.graphics.lineTo(
          50 + Math.cos(Math.atan2(3,2)) * 34,
          50 - Math.sin(Math.atan2(3,2)) * 34
        );
        this.graphics.moveTo(
          50 - Math.cos(Math.atan2(3,2)) * 24,
          50 - Math.sin(Math.atan2(3,2)) * 24
        );
        this.graphics.lineTo(
          50 - Math.cos(Math.atan2(3,2)) * 34,
          50 - Math.sin(Math.atan2(3,2)) * 34
        );
      }
      if(possiblePieces.includes('b')) {
        this.graphics.drawRegularPolygon(50, 50, 38 / Math.SQRT2, 4, Math.PI / 2);
      }
      if(possiblePieces.includes('r')) {
        this.graphics.drawRect(50 - 19, 50 - 19, 38, 38);
      }
      if(possiblePieces.includes('q')) {
        this.graphics.drawRegularPolygon(50, 50, 12, 8, Math.PI / 8);
      }
      if(possiblePieces.includes('k')) {
        this.graphics.moveTo(50, 50 - 6);
        this.graphics.lineTo(50, 50 + 6);
        this.graphics.moveTo(50 - 6, 50);
        this.graphics.lineTo(50 + 6, 50);
      }
      this.promotedGraphics.clear();
      if(this.hasPromoted) {
        this.promotedGraphics.lineStyle({
          width: 3.8,
          color: isWhite ? 0x000000 : 0xffffff,
          cap: PIXI.LINE_CAP.ROUND,
          join: PIXI.LINE_JOIN.ROUND
        });
        this.promotedGraphics.beginFill(isWhite ? 0xffffff : 0x000000);
        this.promotedGraphics.drawCircle(50, 50, 45);
        this.promotedGraphics.endFill();
        let polygons = [];
        for(let ang = 0;ang < 2 * Math.PI;ang += Math.PI / 8) {
          polygons.push({
            x: Math.sin(ang) * 34,
            y: Math.cos(ang) * 34,
          });
        }
        this.promotedGraphics.drawDashedPolygon(polygons, 50, 50, 0, 3, 12, 0);
        if(possiblePromotedPieces.includes('n')) {
          this.promotedGraphics.moveTo(
            50 + Math.cos(Math.atan2(2,3)) * 24,
            50 + Math.sin(Math.atan2(2,3)) * 24
          );
          this.promotedGraphics.lineTo(
            50 + Math.cos(Math.atan2(2,3)) * 34,
            50 + Math.sin(Math.atan2(2,3)) * 34
          );
          this.promotedGraphics.moveTo(
            50 - Math.cos(Math.atan2(2,3)) * 24,
            50 + Math.sin(Math.atan2(2,3)) * 24
          );
          this.promotedGraphics.lineTo(
            50 - Math.cos(Math.atan2(2,3)) * 34,
            50 + Math.sin(Math.atan2(2,3)) * 34
          );
          this.promotedGraphics.moveTo(
            50 + Math.cos(Math.atan2(2,3)) * 24,
            50 - Math.sin(Math.atan2(2,3)) * 24
          );
          this.promotedGraphics.lineTo(
            50 + Math.cos(Math.atan2(2,3)) * 34,
            50 - Math.sin(Math.atan2(2,3)) * 34
          );
          this.promotedGraphics.moveTo(
            50 - Math.cos(Math.atan2(2,3)) * 24,
            50 - Math.sin(Math.atan2(2,3)) * 24
          );
          this.promotedGraphics.lineTo(
            50 - Math.cos(Math.atan2(2,3)) * 34,
            50 - Math.sin(Math.atan2(2,3)) * 34
          );
          this.promotedGraphics.moveTo(
            50 + Math.cos(Math.atan2(3,2)) * 24,
            50 + Math.sin(Math.atan2(3,2)) * 24
          );
          this.promotedGraphics.lineTo(
            50 + Math.cos(Math.atan2(3,2)) * 34,
            50 + Math.sin(Math.atan2(3,2)) * 34
          );
          this.promotedGraphics.moveTo(
            50 - Math.cos(Math.atan2(3,2)) * 24,
            50 + Math.sin(Math.atan2(3,2)) * 24
          );
          this.promotedGraphics.lineTo(
            50 - Math.cos(Math.atan2(3,2)) * 34,
            50 + Math.sin(Math.atan2(3,2)) * 34
          );
          this.promotedGraphics.moveTo(
            50 + Math.cos(Math.atan2(3,2)) * 24,
            50 - Math.sin(Math.atan2(3,2)) * 24
          );
          this.promotedGraphics.lineTo(
            50 + Math.cos(Math.atan2(3,2)) * 34,
            50 - Math.sin(Math.atan2(3,2)) * 34
          );
          this.promotedGraphics.moveTo(
            50 - Math.cos(Math.atan2(3,2)) * 24,
            50 - Math.sin(Math.atan2(3,2)) * 24
          );
          this.promotedGraphics.lineTo(
            50 - Math.cos(Math.atan2(3,2)) * 34,
            50 - Math.sin(Math.atan2(3,2)) * 34
          );
        }
        if(possiblePromotedPieces.includes('b')) {
          this.promotedGraphics.drawRegularPolygon(50, 50, 38 / Math.SQRT2, 4, Math.PI / 2);
        }
        if(possiblePromotedPieces.includes('r')) {
          this.promotedGraphics.drawRect(50 - 19, 50 - 19, 38, 38);
        }
        if(possiblePromotedPieces.includes('q')) {
          this.promotedGraphics.drawRegularPolygon(50, 50, 12, 8, Math.PI / 8);
        }
      }
      this.root.visible = true;
    }
    else {
      this.root.visible = false;
      this.possibilityLength = 0;
    }
  }
  listen() {
    this.graphics.on('pointertap', () => {
      this.emitter.emit('pieceSelect', this.id);
    });

    PIXI.Ticker.shared.add((delta) => {
      if(this.hasPromoted) {
        const promotionDuration = 1500;
        const promotionCyclePercent = (Date.now() % promotionDuration) / promotionDuration;
        if(this.hasNonPromoted) {
          this.promotedGraphics.filters[0].alpha = Math.sin(promotionCyclePercent * Math.PI);
        }
        else {
          this.promotedGraphics.filters[0].alpha = 1;
        }
      }
      else {
        this.promotedGraphics.filters[0].alpha = 0;
      }

      /*
      if(this.possibilityLength <= 1) {
        this.root.filters[0].red = [0,0];
        this.root.filters[0].green = [0,0];
        this.root.filters[0].blue = [0,0];
      }
      else {
        if(!Array.isArray(this.target) || !Array.isArray(this.source)) {
          this.source = [0,0,0,0,0,0];
          this.target = [
            0.5 - (1*Math.random()),
            0.5 - (1*Math.random()),
            0.5 - (1*Math.random()),
            0.5 - (1*Math.random()),
            0.5 - (1*Math.random()),
            0.5 - (1*Math.random()),
          ];
        }
        if(
          typeof this.rgbSplitLeft !== 'number' ||
          typeof this.rgbSplitDuration !== 'number' ||
          !Array.isArray(this.target) ||
          this.rgbSplitLeft <= 0
        ) {
          this.rgbSplitDuration = 200 / this.possibilityLength;
          this.rgbSplitLeft = this.rgbSplitDuration;
          this.source = this.target.slice();
          this.target = [
            0.5 - (1*Math.random()),
            0.5 - (1*Math.random()),
            0.5 - (1*Math.random()),
            0.5 - (1*Math.random()),
            0.5 - (1*Math.random()),
            0.5 - (1*Math.random()),
          ];
        }
        this.rgbSplitLeft -= PIXI.Ticker.shared.deltaMS;
        this.rgbPercentage = this.rgbSplitLeft / this.rgbSplitDuration;
        this.root.filters[0].red[0] = (this.target[0] - this.source[0]) * this.rgbPercentage;
        this.root.filters[0].red[1] = (this.target[1] - this.source[1]) * this.rgbPercentage;
        this.root.filters[0].green[0] = (this.target[2] - this.source[2]) * this.rgbPercentage;
        this.root.filters[0].green[1] = (this.target[3] - this.source[3]) * this.rgbPercentage;
        this.root.filters[0].blue[0] = (this.target[4] - this.source[4]) * this.rgbPercentage;
        this.root.filters[0].blue[1] = (this.target[5] - this.source[5]) * this.rgbPercentage;
      }
      */
    });
  }
}