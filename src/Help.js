import * as PIXI from 'pixi.js';
import { createNanoEvents } from 'nanoevents';
import Piece from './renderer/Piece';
import Board from './renderer/Board';
import { Client } from 'boardgame.io/client';
import { EntanglementChess } from './Game';

const emitter = createNanoEvents();

/*
const display1Element = document.getElementById('display1');
const display1App = new PIXI.Application({ width: 900, height: 200, backgroundColor: 0x2b443c });
display1Element.appendChild(display1App.view);
new Piece(emitter, 0, 0, display1App.stage, '', ['P']);
new Piece(emitter, 100, 0, display1App.stage, '', ['N']);
new Piece(emitter, 200, 0, display1App.stage, '', ['B']);
new Piece(emitter, 300, 0, display1App.stage, '', ['R']);
new Piece(emitter, 400, 0, display1App.stage, '', ['Q']);
new Piece(emitter, 500, 0, display1App.stage, '', ['K']);
new Piece(emitter, 0, 100, display1App.stage, '', ['p']);
new Piece(emitter, 100, 100, display1App.stage, '', ['n']);
new Piece(emitter, 200, 100, display1App.stage, '', ['b']);
new Piece(emitter, 300, 100, display1App.stage, '', ['r']);
new Piece(emitter, 400, 100, display1App.stage, '', ['q']);
new Piece(emitter, 500, 100, display1App.stage, '', ['k']);
*/

const display2Element = document.getElementById('display2');
const display2App = new PIXI.Application({ width: 300, height: 200, backgroundColor: 0x2b443c });
display2Element.appendChild(display2App.view);
new Piece(emitter, 0, 0, display2App.stage, '', ['P','B','Q']);
new Piece(emitter, 100, 0, display2App.stage, '', ['R','K']);
new Piece(emitter, 200, 0, display2App.stage, '', ['Q','K',['N','B','R','Q']]);
new Piece(emitter, 0, 100, display2App.stage, '', ['p','b','q']);
new Piece(emitter, 100, 100, display2App.stage, '', ['r','k']);
new Piece(emitter, 200, 100, display2App.stage, '', ['q','k',['n','b','r','q']]);

const moveListBoardGen = (emitter, elementId, moveList) => {
  const rootElement = document.getElementById(elementId);
  const app = new PIXI.Application({ width: 1240, height: 820, backgroundColor: 0x2b443c });
  rootElement.appendChild(app.view);
  const client = Client({ game: EntanglementChess, debug: false });
  for(const move of moveList) {
    client.moves.movePiece(move);
  }
  let state = client.getState();
  new Board(emitter, app.stage, state.G, state.ctx);
}

/*
moveListBoardGen(emitter, 'knights1', [
  'b1c3',
  'b8c6',
  'g1f3'
]);

moveListBoardGen(emitter, 'qr1', [
  'a2a5',
  'h7h4',
  'b2b5',
  'g7g4',
  'c2c5'
]);

moveListBoardGen(emitter, 'qr2', [
  'a2a5',
  'h7h4',
  'b2b5',
  'g7g4',
  'c2c5',
  'b8c6',
  'a5b4'
]);
*/